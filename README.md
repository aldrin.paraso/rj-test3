# php7-dockerized
Simple PHP7 Docker &amp; Compose Environment 

In order to assist in teaching the building of simple PHP applications and understanding the use of docker and compose this repository was adapted from [Kasper Isager's PHP Dockerized](https://github.com/kasperisager/php-dockerized)

## Technology included

* [Nginx](http://nginx.org/)
* [MySQL](http://www.mysql.com/)
* [PHP 7](http://php.net/)

## Requirements

* [Docker Native](https://www.docker.com/products/overview)

## Running

Clone the repository.
Change directory into the cloned project.
Run the following command.

```sh
$ docker-compose up
```

Open your browser and type 'localhost' and you'll find the application running on the localhost.
